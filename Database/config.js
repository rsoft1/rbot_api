let MongoClient = require('mongodb').MongoClient;
let uri = require('../Conf/index').mogoURI;

try {

    var client = new MongoClient(uri, { useNewUrlParser: true });
    client.connect(err => {
        if (err)
            throw err;

        console.log("Database is connected");
    });

} catch (err) {
    console.log('error al conectar a la DB :', err.message);
}


module.exports = client;