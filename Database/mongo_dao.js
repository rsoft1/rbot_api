const client = require(`./config`);

module.exports = {

    create_collection: async(db, collection) => {
        var result = await client.db(db).collection(collection).insertOne({ test: 'test_data' });

        if (result) {
            return true;
        } else {
            return false;
        }
    },

    insert_one: async(db, collection, doc) => {
        var result = await client.db(db).collection(collection).insertOne(doc);

        if (result) {
            return true;
        } else {
            return false;
        }
    },

    insert_many: async(db, collection, docs) => {
        var result = await client.db(db).collection(collection).insertMany(docs);

        if (result) {
            return true;
        } else {
            return false;
        }
    },

    find_one: async(db, collection, query, aggregate = null) => {

        if (aggregate) {
            var result = await client.db(db).collection(collection).aggregate(aggregate).toArray();
        } else {
            var result = await client.db(db).collection(collection).findOne(query);
        }

        if (result) {
            return result[0];
        } else {
            return null;
        }
    },

    find: async(db, collection, query, sort = { last_review: -1 }, limit = Number.MAX_SAFE_INTEGER, join = null) => {
        var result = await client.db(db)
            .collection(collection)
            .find(query)
            .sort(sort)
            .limit(limit)
            .toArray();

        if (result) {
            return result;
        } else {
            return null;
        }
    },

    update_one: async(db, collection, find, update) => {
        var result = await client.db(db)
            .collection(collection)
            .updateOne(find, { $set: update });

        if (result) {
            return true;
        } else {
            return false;
        }
    },

    delete_one: async(db, collection, find) => {
        var result = await client.db(db)
            .collection(collection)
            .deleteOne(find);

        if (result) {
            return true;
        } else {
            return false;
        }
    }

}