module.exports = {

  response: (res, status, msg, data) => {
    res.status(status).json({
        status: status,
        msg: msg,
        data: data
    });
  }

}
