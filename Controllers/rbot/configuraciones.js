const mongoDAO = require('../../Database/mongo_dao');
var ObjectId = require('mongodb').ObjectID;
const GF = require('../global');

module.exports = {


    update_configuration: async(req, res, next) => {
        try {

            var id = req.params.id

            var configuration = req.body.configuration

            var update = await mongoDAO.update_one(req.user.instancia, 'Configuraciones', { _id: ObjectId(id) }, configuration);

            if (update) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear la configuracion', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    get_configurations: async(req, res, next) => {
        try {

            var id = req.params.id;

            var configuracion = await mongoDAO.find(req.user.instancia, 'Configuraciones', { _id: ObjectId(id) });

            if (configuracion) {
                GF.response(res, 200, 'ok_', configuracion);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    }

}