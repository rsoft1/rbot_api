const mongoDAO = require('../../Database/mongo_dao');
var ObjectId = require('mongodb').ObjectID;
const GF = require('../global');


module.exports = {

    get_history: async(req, res, next) => {
        try {

            var historial = await mongoDAO.find(req.user.instancia, 'Historial', {});

            if (historial) {
                GF.response(res, 200, 'ok_', historial);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    get_history_details: async(req, res, next) => {
        try {

            var id = req.params.id;

            var historial = await mongoDAO.find(req.user.instancia, 'Historial', {});

            if (historial) {
                GF.response(res, 200, 'ok_', historial);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    create_history: async(req, res, next) => {
        try {

            var history = req.body.history;

            var historial = await mongoDAO.insert_one(req.user.instancia, 'Historial', history);

            if (historial) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el registro', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    create_list_history: async(req, res, next) => {
        try {

            var historys = req.body.historys;

            var historial = await mongoDAO.insert_many(req.user.instancia, 'Historial', historys);

            if (historial) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el registro', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    }

}