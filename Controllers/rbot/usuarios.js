const mongoDAO = require('../../Database/mongo_dao');
var ObjectId = require('mongodb').ObjectID;
const GF = require('../global');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const KEY_SECRET = '@.|R6b60t|.@';

module.exports = {

    login: async(req, res, next) => {
        try {

            var username = req.body.username;
            var password = req.body.password

            var user = await mongoDAO.find_one('RDB_MASTER', 'Usuarios', {}, [{
                '$match': {
                    'username': username
                }
            }, {
                '$lookup': {
                    'from': 'Permisos',
                    'localField': 'permissions',
                    'foreignField': '_id',
                    'as': 'permisos'
                }
            }, {
                '$lookup': {
                    'from': 'Instancias',
                    'localField': 'instance',
                    'foreignField': '_id',
                    'as': 'instancia'
                }
            }, {
                '$project': {
                    'permissions': 0,
                    'instance': 0
                }
            }]);


            if (user != null && user.active) {


                if (user.permisos[0] && user.instancia[0] && user.instancia[0].active) {


                    const resultPassword = bcrypt.compareSync(password, user.password);
                    if (resultPassword) {
                        const expiresIn = 24 * 60 * 60;
                        const accessToken = jwt.sign({ user: { _id: user._id, instancia: user.instancia[0].name, permisos: user.permisos[0].access } }, KEY_SECRET, { expiresIn: expiresIn });
                        const dataUser = {
                            username: user.username,
                            email: user.email,
                            accessToken: accessToken
                        }
                        GF.response(res, 200, "ok_", dataUser);
                    } else {
                        GF.response(res, 409, "Credenciales incorrectas", []);
                    }
                } else {
                    var mensaje = !user.permisos[0] ? 'No se encontraron los permisos de este usuario' : !user.instancia[0] ? 'No se encontro la instancia de este usuario' : !user.instancia[0].active ? 'Esta instancia no se encuentra activa' : 'Error desconocido';
                    GF.response(res, 409, mensaje, []);
                }


            } else {
                var mensaje = !user ? 'No se encontro ningun usuario' : !user.active ? 'Este usuario esta deshabilitado' : 'Error desconocido';
                GF.response(res, 409, mensaje, []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    update_user: async(req, res, next) => {
        try {
            var username = req.body.username ? req.body.username : req.user.username;
            var password = req.body.password ? req.body.password : req.user.password;
            var email = req.body.email ? req.body.email : req.user.email;
            var oldPass = req.body.oldPass;

            const resultPassword = bcrypt.compareSync(oldPass, req.user.password);
            var passwordHash = await bcrypt.hashSync(password);
            if (resultPassword) {

                var result = await mongoDAO.update_one('RDB_MASTER', 'Usuarios', { _id: ObjectId(req.user._id) }, { username: username, password: passwordHash, email: email, active: true })

                if (result) {
                    GF.response(res, 200, "ok_", []);
                } else {
                    GF.response(res, 409, "No se pudo actualizar", []);
                }

            } else {
                GF.response(res, 409, "Credenciales incorrectas", []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    create_user: async(req, res, next) => {
        try {

            var user = req.body.user;

            var create = await mongoDAO.insert_one('RDB_MASTER', 'Usuarios', user);

            if (create) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    update_user_admin: async(req, res, next) => {
        try {

            var id = req.params.id

            var user = req.body.user

            var update = await mongoDAO.update_one('RDB_MASTER', 'Usuarios', { _id: ObjectId(id) }, user);

            if (update) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    delete_user: async(req, res, next) => {
        try {

            var id = req.params.id

            var deletes = await mongoDAO.delete_one('RDB_MASTER', 'Usuarios', { _id: ObjectId(id) });

            if (deletes) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo eliminar el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    list_users: async(req, res, next) => {
        try {

            var contacts = await mongoDAO.find('RDB_MASTER', 'Usuarios', {});

            if (contacts) {
                GF.response(res, 200, 'ok_', contacts);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    }

}