const mongoDAO = require('../../Database/mongo_dao');
var ObjectId = require('mongodb').ObjectID;
const GF = require('../global');

module.exports = {

    create_contact: async(req, res, next) => {
        try {

            var contact = req.body.contact;

            var create = await mongoDAO.insert_one(req.user.instancia, 'Contactos', contact);

            if (create) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    create_list_contact: async(req, res, next) => {
        try {

            var contacts = req.body.contacts

            var create = await mongoDAO.insert_many(req.user.instancia, 'Contactos', contacts);

            if (create) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    update_contact: async(req, res, next) => {
        try {

            var id = req.params.id

            var contact = req.body.contact

            var update = await mongoDAO.update_one(req.user.instancia, 'Contactos', { _id: ObjectId(id) }, contact);

            if (update) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    delete_contact: async(req, res, next) => {
        try {

            var id = req.params.id

            var deletes = await mongoDAO.delete_one(req.user.instancia, 'Contactos', { _id: ObjectId(id) });

            if (deletes) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo eliminar el contacto', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    list_contact: async(req, res, next) => {
        try {

            var contacts = await mongoDAO.find(req.user.instancia, 'Contactos', {});

            if (contacts) {
                GF.response(res, 200, 'ok_', contacts);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    }

}