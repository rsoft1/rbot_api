const mongoDAO = require('../../Database/mongo_dao');
var ObjectId = require('mongodb').ObjectID;
const GF = require('../global');

module.exports = {

    create_instance: async(req, res, next) => {
        try {

            var instance = req.body.instance;

            var create = await mongoDAO.insert_one('RDB', 'Instancia', instance);

            if (create) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el instanceo', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    update_instance: async(req, res, next) => {
        try {

            var id = req.params.id

            var instance = req.body.instance

            var update = await mongoDAO.update_one('RDB', 'Instancias', { _id: ObjectId(id) }, instance);

            if (update) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el instanceo', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    delete_instance: async(req, res, next) => {
        try {

            var id = req.params.id

            var deletes = await mongoDAO.delete_one('RDB', 'Instancias', { _id: ObjectId(id) });

            if (deletes) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo eliminar el instanceo', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    list_instance: async(req, res, next) => {
        try {

            var instances = await mongoDAO.find('RDB', 'Instancias', {});

            if (instances) {
                GF.response(res, 200, 'ok_', instances);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    }

}