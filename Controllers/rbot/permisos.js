const mongoDAO = require('../../Database/mongo_dao');
var ObjectId = require('mongodb').ObjectID;
const GF = require('../global');

module.exports = {

    create_permissions: async(req, res, next) => {
        try {

            var permissions = req.body.permissions;

            var create = await mongoDAO.insert_one('RDB', 'Permisos', permissions);

            if (create) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el permissionso', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    update_permissions: async(req, res, next) => {
        try {

            var id = req.params.id

            var permissions = req.body.permissions

            var update = await mongoDAO.update_one('RDB', 'Permisos', { _id: ObjectId(id) }, permissions);

            if (update) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo crear el permissionso', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    delete_permissions: async(req, res, next) => {
        try {

            var id = req.params.id

            var deletes = await mongoDAO.delete_one('RDB', 'Permisos', { _id: ObjectId(id) });

            if (deletes) {
                GF.response(res, 200, 'ok_', []);
            } else {
                GF.response(res, 409, 'No se pudo eliminar el permissionso', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    },

    list_permissions: async(req, res, next) => {
        try {

            var permissionss = await mongoDAO.find('RDB', 'Permisos', {});

            if (permissionss) {
                GF.response(res, 200, 'ok_', permissionss);
            } else {
                GF.response(res, 409, 'No hay registros', []);
            }

        } catch (error) {
            console.log(error);
            GF.response(res, 500, 'err_', error);
        }
    }

}