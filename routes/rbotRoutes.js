const express = require('express');
const router = express.Router();
const middles = require('../middlewares/middles');
const RbotController = require('../Controllers/rbot');

/**LOGIN ROUTE */
router.post('/rbot/login', RbotController.usuarios.login);

/**USUARIOS ROUTES */
router.get('/rbot/usuarios', [middles.accessToken], RbotController.usuarios.list_users);
router.post('/rbot/usuarios', [middles.accessToken], RbotController.usuarios.create_user);
router.put('/rbot/usuarios', [middles.accessToken], RbotController.usuarios.update_user);
router.put('/rbot/usuarios/:id', [middles.accessToken], RbotController.usuarios.update_user_admin);
router.delete('/rbot/usuarios/:id', [middles.accessToken], RbotController.usuarios.delete_user);

/**HISTORIAL ROUTES */
router.get('/rbot/historial', [middles.accessToken], RbotController.historial.get_history);
router.get('/rbot/historial/detalles/:id', [middles.accessToken], RbotController.historial.get_history_details);
router.post('/rbot/historial', [middles.accessToken], RbotController.historial.create_history);
router.post('/rbot/historial/detalles/:id', [middles.accessToken], RbotController.historial.create_list_history);

/** CONTACTOS ROUTES */
router.get('/rbot/contactos', [middles.accessToken], RbotController.contactos.list_contact);
router.post('/rbot/contactos_list', [middles.accessToken], RbotController.contactos.create_list_contact);
router.post('/rbot/contactos', [middles.accessToken], RbotController.contactos.create_contact);
router.put('/rbot/contactos/:id', [middles.accessToken], RbotController.contactos.update_contact);
router.delete('/rbot/contactos/:id', [middles.accessToken], RbotController.contactos.delete_contact);

/**CONFIGURACION ROUTES */
router.get('/rbot/configuracion/:id', [middles.accessToken], RbotController.configuraciones.get_configurations);
router.post('/rbot/configuracion', [middles.accessToken], RbotController.configuraciones.update_configuration);

/** PERMISOS ROUTES */
router.get('/rbot/permisos', [middles.accessToken], RbotController.permisos.list_permissions);
router.post('/rbot/permisos', [middles.accessToken], RbotController.permisos.create_permissions);
router.put('/rbot/permisos/:id', [middles.accessToken], RbotController.permisos.update_permissions);
router.delete('/rbot/permisos/:id', [middles.accessToken], RbotController.permisos.delete_permissions);

/** INSTANCIAS ROUTES */
router.get('/rbot/instancias', [middles.accessToken], RbotController.instancias.list_instance);
router.post('/rbot/instancias', [middles.accessToken], RbotController.instancias.create_instance);
router.put('/rbot/instancias/:id', [middles.accessToken], RbotController.instancias.update_instance);
router.delete('/rbot/instancias/:id', [middles.accessToken], RbotController.instancias.delete_instance);


module.exports = router;