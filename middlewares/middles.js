const jwt = require('jsonwebtoken');
const KEY_SECRET = '@.|R6b60t|.@';


module.exports = {

    accessToken: async(req, res, next) => {

        var token = req.headers["x-access-token"] || req.headers["authorization"];

        if (!token) {
            if (!req.body.token) {
                return res.status(401).send("Access denied. No token provided.");
            } else {
                token = req.body.token;
            }

        }

        try {
            jwt.verify(token, KEY_SECRET, (err, data) => {
                if (!data) return res.status(401).send("Token expired");

                if (!data.user) {
                    return res.status(401).send("Invalid Token");
                }

                req.user = data.user;
                next();
            });
        } catch (ex) {
            console.log(ex);
            res.status(400).send("Invalid token.");
        }
    }

}