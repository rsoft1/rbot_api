"use strict";
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

/*IMPORT ROUTES*/
const rbotRoutes = require('./routes/rbotRoutes');


const morgan = require('morgan');
const cors = require('cors');

const CONFIG = require('./Conf/index');

const errorhandler = require('errorhandler');
const EventEmitter = require('events');
class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter();

// increase the limit of conecctions
myEmitter.setMaxListeners(0);
require('events').EventEmitter.defaultMaxListeners = Infinity;
const app = express();

app.use(cors());

app.use(bodyParser.json({ limit: '20000kb' }));
app.disable('x-powered-by'); //Remove the X-powered- by from the header in this case X-powered- by: express

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cookieParser())
app.use(morgan('dev'));

app.use('/api/', [rbotRoutes]);

let express_enforces_ssl = require('express-enforces-ssl');


if (CONFIG.enforceSSL === 'yes') {
    app.use(express_enforces_ssl());
}
app.use((req, res, next) => {
    //configurar cabecera
    //para permitir el acceso a nuestra api de todos los dominios
    res.header('Access-Control-Allow-Origin', '*');
    //cabeceras necesarias para AJAX
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    //para salir del flujo y seguir
    next();
});


app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    app.use(errorhandler());

    //  the error page
    res.status(err.status || 500)
        //.send('Something is wrong!');;
});

module.exports = app;